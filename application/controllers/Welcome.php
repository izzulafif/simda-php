<?php

use Distefano\Simda\Dpa;
use Distefano\Simda\Master;
use Distefano\Bkd\Statistik;
use Distefano\Simoneva\Transfer;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $m = new Master();
        dd($m->perubahan());
        $s = new Transfer();
        dd($s->realisasi());
        $ss = new Statistik();
        dd($ss->all());
        $data = new Dpa;
        dd($data->ref());
        var_dump($data->ref());
        exit;
		$this->load->view('welcome_message');
	}
}
