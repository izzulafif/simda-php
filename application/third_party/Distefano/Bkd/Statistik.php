<?php

namespace Distefano\Bkd;

use Distefano\Config;

class Statistik
{
    protected $ci;
    protected $db;
    protected $db_connection = 'bkd';

    public function __construct()
    {
        $this->ci =& get_instance();
        $connection = Config::db($this->db_connection);
        $this->db = $this->ci->load->database($connection, TRUE);
    }

    public function all()
    {
        $data = $this->db->query("Select * from dbo.fntbDetailPegawai('19730501 200604 1 019')");
        return $data->result();
    }

    

}

/* End of file Statistik.php */
/* Location: ./application/third_party/Distefano/Bkd/Statistik.php */
