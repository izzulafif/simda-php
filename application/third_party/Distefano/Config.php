<?php

namespace Distefano;

class Config
{
    public static function db($config='local')
    {
        $db = require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config' 
            . DIRECTORY_SEPARATOR . 'db.php';

        return $db[$config];
    }

    
}

/* End of file Config.php */
/* Location: ./application/third_party/Distefano/Config.php */
