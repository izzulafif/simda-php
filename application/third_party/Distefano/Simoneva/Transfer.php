<?php

namespace Distefano\Simoneva;

use Distefano\Config;

class Transfer
{
    protected $ci;
    protected $db;
    protected $db_connection = 'local';

    public function __construct()
    {
        $this->ci =& get_instance();
        $connection = Config::db($this->db_connection);
        $this->db = $this->ci->load->database($connection, TRUE);
    }

    public function realisasi()
    {
        $data = $this->db->get('bud')->result();
        // return $data;
        $temps = array_map(function ($x) {
            $d = new \stdClass;
            $d->kd_urusan = substr($x->KodeUnit, 0,1);
            $d->kd_bidang = substr($x->KodeUnit, 2,2);
            $d->kd_unit = substr($x->KodeUnit, 5,2);
            $d->kd_sub_unit = substr($x->KodeUnit, 8,2);
            $d->kd_urusan1 = substr($x->Kode_Kegiatan, 3,1);
            $d->kd_bidang1 = substr($x->Kode_Kegiatan, 4,2);
            $d->kd_program = substr($x->Kode_Kegiatan, 0,2);
            $d->kd_kegiatan = substr($x->Kode_Kegiatan, -2);
            $d->realisasi_tw3 = $x->TOTAL_REALISASI;
            return $d;
        }, $data);
        $this->db->insert_batch('temps2', $temps);
        return $temps;        
    }

    public function step2($data)
    {
        foreach ($data as $v) {
            if ($v->kd_sub_unit = 1) {
                $v->kd_sub_unit = $v->kd_sub_unit;
            } else if ($v->kd_urusan = 4 && $v->kd_bidang = 1 && $v->kd_unit = 3) {
                $v->kd_sub_unit = $v->kd_sub_unit;
            } else if ($v->kd_urusan = 1 && $v->kd_bidang = 2 && $v->kd_unit = 2) {
                $v->kd_sub_unit = $v->kd_sub_unit;
            } else {
                $v->kd_sub_unit = 1;
            }
            $this->db->insert('temps2', $v);
            
        }


        return $data;
    }

}

/* End of file Transfer.php */
/* Location: ./application/third_party/Distefano/Simoneva/Transfer.php */
