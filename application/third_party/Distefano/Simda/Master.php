<?php

namespace Distefano\Simda;

use Distefano\Config;

class Master
{
    protected $ci;
    protected $db;
    protected $db_connection = 'simda';

    public function __construct()
    {
        $this->ci =& get_instance();
        $connection = Config::db($this->db_connection);
        $this->db = $this->ci->load->database($connection, TRUE);
    }

    public function perubahan($kode = FALSE)
    {
        if (! $kode) {
            return $this->db->get('Ref_Perubahan')->result();
        }

        $this->db->where('Kd_Perubahan', $kode);
        return $this->db->get('Ref_Perubahan')->result();
    }

    public function fungsi($kode = FALSE)
    {
        if (! $kode) {
            return $this->db->get('Ref_Fungsi')->result();
        }

        $this->db->where('Kd_Fungsi', $kode);
        return $this->db->get('Ref_Fungsi')->result();
    }

    public function urusan($kode = FALSE)
    {
        if (! $kode) {
            return $this->db->get('Ref_Urusan')->result();
        }

        $this->db->where('Kd_Urusan', $kode);
        return $this->db->get('Ref_Urusan')->result();
    }

    public function bidang($urusan = FALSE, $bidang = FALSE)
    {
        if (! $urusan && ! $bidang) {
            return $this->db->get('ref_bidang')->result();
        }

        if (! $urusan) {
            $this->db->where('Kd_Bidang', $bidang);
            return $this->db->get('ref_bidang')->result();
        }

        if (! $bidang) {
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('ref_bidang')->result();
        }

        $this->db->where('Kd_Bidang', $bidang);
        $this->db->where('Kd_Urusan', $urusan);
        return $this->db->get('ref_urusan')->result();
    }

    public function unit($urusan = FALSE, $bidang = FALSE, $unit = FALSE)
    {
        if (! $urusan && ! $bidang && ! $unit) {
            return $this->db->get('ref_unit')->result();
        }

        if (! $urusan && ! $bidang) {
            $this->db->where('Kd_Unit', $unit);
            return $this->db->get('ref_unit')->result();
        }

        if (! $urusan && ! $unit) {
            $this->db->where('Kd_Bidang', $bidang);
            return $this->db->get('ref_unit')->result();
        }

        if (! $bidang && ! $unit) {
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('ref_unit')->result();
        }

        if (! $bidang ) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Unit', $unit);
            return $this->db->get('ref_unit')->result();
        }

        if (! $unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('ref_unit')->result();
        }

        if (! $urusan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Unit', $unit);
            return $this->db->get('ref_unit')->result();
        }

        $this->db->where('Kd_Bidang', $bidang);
        $this->db->where('Kd_Urusan', $urusan);
        $this->db->where('Kd_Unit', $unit);
        return $this->db->get('ref_unit')->result();
    }

    public function program($urusan = FALSE, $bidang = FALSE, $program = FALSE)
    {
        if (! $urusan && ! $bidang && ! $program) {
            return $this->db->get('Ref_Program')->result();
        }

        if (! $urusan && ! $bidang) {
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Program')->result();
        }

        if (! $urusan && ! $program) {
            $this->db->where('Kd_Bidang', $bidang);
            return $this->db->get('Ref_Program')->result();
        }

        if (! $bidang && ! $program) {
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Program')->result();
        }

        if (! $bidang ) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Program')->result();
        }

        if (! $program) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Program')->result();
        }

        if (! $urusan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Program')->result();
        }

        $this->db->where('Kd_Bidang', $bidang);
        $this->db->where('Kd_Urusan', $urusan);
        $this->db->where('Kd_Prog', $program);
        return $this->db->get('Ref_Program')->result();
    }

    public function kegiatan($urusan = FALSE, $bidang = FALSE, $program = FALSE, $kegiatan = FALSE)
    {
        if (! $urusan && ! $bidang && ! $program && ! $kegiatan) {
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan && ! $bidang && ! $program) {
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }
        
        if (! $urusan && ! $bidang && ! $kegiatan) {
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan && ! $program && ! $kegiatan) {
            $this->db->where('Kd_Bidang', $bidang);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $bidang && ! $program && ! $kegiatan) {
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan && ! $bidang) {
            $this->db->where('Kd_Prog', $program);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan && ! $program) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan && ! $kegiatan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $bidang && ! $program) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $bidang && ! $kegiatan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $program && ! $kegiatan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $urusan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Prog', $program);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $bidang) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Prog', $program);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $kegiatan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Prog', $program);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        if (! $program) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Keg', $kegiatan);
            return $this->db->get('Ref_Kegiatan')->result();
        }

        $this->db->where('Kd_Bidang', $bidang);
        $this->db->where('Kd_Urusan', $urusan);
        $this->db->where('Kd_Prog', $program);
        $this->db->where('Kd_Keg', $kegiatan);
        return $this->db->get('Ref_Kegiatan')->result();
    }

    public function sub_unit($urusan = FALSE, $bidang = FALSE, $unit = FALSE, $sub_unit = FALSE)
    {
        if (! $urusan && ! $bidang && ! $unit && ! $sub_unit) {
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan && ! $bidang && ! $unit) {
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }
        
        if (! $urusan && ! $bidang && ! $sub_unit) {
            $this->db->where('Kd_unit', $unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan && ! $unit && ! $sub_unit) {
            $this->db->where('Kd_Bidang', $bidang);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $bidang && ! $unit && ! $sub_unit) {
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan && ! $bidang) {
            $this->db->where('Kd_unit', $unit);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan && ! $unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan && ! $sub_unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_unit', $unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $bidang && ! $unit) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $bidang && ! $sub_unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $unit && ! $sub_unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $urusan) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_unit', $unit);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $bidang) {
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_unit', $unit);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $sub_unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_unit', $unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        if (! $unit) {
            $this->db->where('Kd_Bidang', $bidang);
            $this->db->where('Kd_Urusan', $urusan);
            $this->db->where('Kd_Sub', $sub_unit);
            return $this->db->get('Ref_Sub_Unit')->result();
        }

        $this->db->where('Kd_Bidang', $bidang);
        $this->db->where('Kd_Urusan', $urusan);
        $this->db->where('Kd_unit', $unit);
        $this->db->where('Kd_Sub', $sub_unit);
        return $this->db->get('Ref_Sub_Unit')->result();
    }
    

}

/* End of file Master.php */
/* Location: ./application/third_party/Distefano/Simda/Master.php */
