<?php

namespace Distefano\Simda;

use Distefano\Config;

class Dpa
{
    protected $ci;
    protected $db;
    protected $db_connection = 'simda';

    public function __construct()
    {
        $this->ci =& get_instance();
        $connection = Config::db($this->db_connection);
        $this->db = $this->ci->load->database($connection, TRUE);
    }

    public function ref()
    {
        $data = $this->db->get('Ref_Unit');

        return $data->result();
    }

}

/* End of file Dpa.php */
/* Location: ./application/third_party/Distefano/Simda/Dpa.php */
